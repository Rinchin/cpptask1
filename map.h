#ifndef MAP
#define MAP

#include "treap.h"

template <typename key_type,
          typename value_type,
          class Compare = less<Key>,
          class Alloc = allocator<pair<const key_type,value_type>>
          >
class Map
{
private:
    typedef Treap<key_type, value_type> TreapType;
    TreapType _treap;
public:

    typedef value_type mappedType;

//    explicit Map (const key_compare& comp = key_compare(),
//                  const allocator_type& alloc = allocator_type());
//    explicit Map (const allocator_type& alloc);
//    Map (const map& x, const allocator_type& alloc);
//    Map (Map&& x);
//    Map (Map&& x, const allocator_type& alloc);
//    Map (initializer_list<value_type> il,
//         const key_compare& comp = key_compare(),
//         const allocator_type& alloc = allocator_type());


    Map(const Map& other) : _treap(other._treap){}
    Map(const Map&& other) :_treap(std::move(other._treap)){}


    template <class InputIterator>
    Map (InputIterator first, InputIterator last,
         const key_compare& comp = key_compare(),
         const allocator_type& = allocator_type()) : Map(){
        insert(first, last);
    }

    ~Map();

    Map& operator=( const Map& other){
        _treap = other._treap;
        return *this;
    }

    Map& operator=( const Map&& other){
        _treap = std::move(other._treap);
        return *this;
    }

    Map<key_type, value_type, Comparator>& operator= (const Map& other);
    Map<key_type, value_type, Comparator>& operator= (const Map&& other);

    class iterator;
    class const_iterator;

    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;




    /*======================Capacity========================*/

    size_t size() const {
        return _treap.Size();
    }
    bool empty() const {
        return 0 == size();
    }

    /*======================Iterators========================*/

    iterator begin() {
        return _treap.Begin();
    }
    const_iterator begin() const {
        return _treap.Begin();
    }

    iterator end() {
        return _treap.End();
    }
    const_iterator end() const {
        return _treap.End();
    }

    iterator find(key_type const& key) {
        return _treap.Find(key);
    }
    const_iterator find(key_type const& key) const {
        return _treap.Find(key);
    }

    value_type& front() {
        return *begin();
    }
    value_type const& front() const {
        return *begin();
    }

    value_type& back();
    value_type const& back() const;


    /*===================modifiers========================*/

//    template <class P> std::pair<iterator,bool> insert (P&& val);
//    template <class P> iterator insert (const_iterator position, P&& val);
//    void insert (initializer_list<value_type> il);

    std::pair<iterator, bool> insert( const value_type & value) {
        return _treap.Insert(value);
    }

    iterator insert(const_iterator position, const value_type& value) {
        if (end() == position) {
            return _treap.Insert(value).first;
        }

        auto foundPosition = _treap.Find(TreapType::GetKey(*position));
        if (end() == foundPosition) {
            //	not found - insert new
            return _treap.Insert(*position).first;
        }
        else {
            if (TreapType::GetKey(value) == TreapType::GetKey(*foundPosition)) {
                TreapType::GetValue(*foundPosition) = TreapType::GetValue(value);
                return foundPosition;
            }
            else {
                //	replace value
                if (!_treap.Erase(TreapType::GetKey(*foundPosition))) {
                    //TODO: fail
                }
                return _treap.Insert(value).first;
            }
        }
    }

    template <class InputIterator>
    void insert(InputIterator first, InputIterator last) {
        while (first != last) {
            _treap.Insert(*first++);
        }
    }

    template <class... Args>
    bool emplace (Args&&... args);


    size_t erase(const_iterator position) {
        return _treap.Erase(TreapType::GetKey(*position));
    }

    iterator erase(ConstIterator first, ConstIterator last) {
        return _treap.Erase(first, last);
    }

    size_t erase(const key_type & key) {
        return _treap.Erase(key);
    }

    void clear() {
        _treap.Clear();
    }



    /*========================Element access=====================*/




    mapped_type& operator[] (const key_type& key) {
        auto result = _treap.Find(key);
        if (end() == result) {
            //	insert not found key with default value
            _treap.Insert(value_type(key, mapped_type()));
        }
        return TreapType::GetValue(*_treap.Find(key));

    }

    mapped_type& operator[] (key_type&& key) {
        auto result = _treap.Find(key);

        if (end() == result) {
            //	insert not found key
            auto insertedIterator = _treap.Insert(ValueType(std::move(key), mapped_type())).first;
            return TreapType::GetValue(*insertedIterator);
        }

        //TODO: check if key-variable is not valid anymore
        return TreapType::GetValue(_treap.Find(key));
    }



};



#endif // MAP

