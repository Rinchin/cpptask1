project(damdinov_map)
cmake_minimum_required(VERSION 2.8) #проверка версии CMake
aux_source_directory(. SRC_LIST)
set(PROJECT damdinov_map)
set(CMAKE_CURRENT_BINARY_DIR ${CMAKE_CURRENT_SOURCE_DIR}/bin)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(SOURCES_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(HEADERS_DIR ${CMAKE_CURRENT_SOURCE_DIR})
file(GLOB_RECURSE SOURCES ${SOURCES_DIR}/*.cpp)
file(GLOB_RECURSE HEADERS ${HEADERS_DIR}/*.h)

add_library(Container ${SOURCES} ${HEADERS})
add_executable(${PROJECT_NAME} ${SRC_LIST}) #создает исполняемый файл с именем проекта


