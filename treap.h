#ifndef TREAP_H
#define TREAP_H

#include <algorithm>
#include <functional>
#include <utility>
#include <iterator>
#include <cstdlib>
#include <stdexcept>


template <
        typename key_type,
        typename value_type,
        typename Comparator = std::less<key_type> >
class Treap
{
private:
    struct Node {
        std::pair<const key_type, value_type> mValue;
        const int mPriority;
        Node* mChildren[2];
        Node* mParent;
        Node* mNext, *mPrev;

        Node(const key_type& key, const value_type& value, int priority)
            : mValue(key, value), mPriority(priority) {}
    };

    Node* mHead, *mTail;
    Node* mRoot;
    Comparator mComp;
    size_t mSize;

public:

    Treap(Comparator comp = Comparator())
        : mComp(comp), mSize(0), mHead(nullptr), mTail(nullptr), mRoot(nullptr){}

    ~Treap() {
        Node* curr = mHead;
        while (curr != nullptr) {
            Node* next = curr->mNext;
            delete curr;/* Free memory, then go to the next node. */
            curr = next;
        }
    }


    Treap(const Treap& other)
    {
        mSize = other.mSize;
        mComp = other.mComp;

        /* Clone the tree structure. */
        mRoot = cloneTree(other.mRoot, NULL);

        /* Rectify the linked list. */
        rethreadLinkedList(mRoot, NULL);

        mTail = mHead = mRoot;
        while (mHead && mHead->mChildren[0]) mHead = mHead->mChildren[0];
        while (mTail && mTail->mChildren[1]) mTail = mTail->mChildren[1];
    }

    Treap(const Treap&& other);


    Treap& operator=( const Treap& other){
        Treap clone = other;
        swap(clone);
        return *this;
    }
    Treap& operator=( const Treap&& other);

    //    Treap<key_type, value_type, Comparator>& operator= (const Treap& other);
    //    Treap<key_type, value_type, Comparator>& operator= (const Treap&& other);

    class iterator;
    class const_iterator;

    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;


    /*===================modifiers========================*/

    std::pair<iterator, bool> insert(const key_type& key, const value_type& value);

    bool erase(const key_type& key);
    iterator erase(iterator where);

    /*===================iterators========================*/

    iterator find(const key_type& key);
    const_iterator find(const key_type& key) const;

    iterator begin();
    const_iterator begin() const;
    reverse_iterator rbegin();
    const_reverse_iterator rbegin() const;

    iterator end();
    const_iterator end() const;
    reverse_iterator rend();
    const_reverse_iterator rend() const;


    /**
     * Usage: for (Treap<string, int>::iterator itr = t.lower_bound("AVL");
     *             itr != t.upper_bound("skiplist"); ++itr) { ... }
     * -------------------------------------------------------------------------
     * lower_bound returns an iterator to the first element in the treap whose
     * key is at least as large as key.  upper_bound returns an iterator to the
     * first element in the treap whose key is strictly greater than key.
     */
    iterator lower_bound(const key_type& key);
    iterator upper_bound(const key_type& key);
    const_iterator lower_bound(const key_type& key) const;
    const_iterator upper_bound(const key_type& key) const;


    /*===================capacity========================*/

    size_t size() const{
        return mSize;
    }
    bool empty() const{
        return size() == 0;
    }
    void swap(Treap& other){
        std::swap(mRoot, other.mRoot);
        std::swap(mSize, other.mSize);
        std::swap(mHead, other.mHead);
        std::swap(mTail, other.mTail);
        std::swap(mComp, other.mComp);
    }

private:
    /* A utility base class for iterator and const_iterator which actually
     * supplies all of the logic necessary for the two to work together.  The
     * parameters are the derived type, the type of a pointer being visited, and
     * the type of a reference being visited.  This uses the Curiously-Recurring
     * Template Pattern to work correctly.
     */
    template <typename DerivedType, typename Pointer, typename Reference>
    class IteratorBase;
    template <typename DerivedType, typename Pointer, typename Reference>
    friend class IteratorBase;

    friend class iterator;
    friend class const_iterator;


    void rotateUp(Node* child);


    /*marked const but returns a non-const so that it can be used in both versions of the find()*/
    Node* findNode(const key_type& key) const;


    static Node* cloneTree(Node* toClone, Node* parent);

    /* A utility function which, given a tree and a pointer to the predecessor
     * of that tree, rewires the linked list in that tree to represent an
     * inorder traversal.  No fields are modified.  The return value is the node
     * with the highest key.
     */
    static Node* rethreadLinkedList(Node* root, Node* predecessor);







};

















template <typename Key, typename Value, typename Comparator>
template <typename DerivedType, typename Pointer, typename Reference>
class Treap<Key, Value, Comparator>::IteratorBase {
public:
    typedef typename Treap<Key, Value, Comparator>::Node Node;

    DerivedType& operator++ () {
        mCurr = mCurr->mNext;
        return static_cast<DerivedType&>(*this);
    }
    const DerivedType operator++ (int) {
        DerivedType result = static_cast<DerivedType&>(*this);
        ++*this;
        return result;/* Hand back the cached value. */
    }

    DerivedType& operator-- () {
        if (mCurr == nullptr) {
            mCurr = mOwner->mTail;
        }
        else {
            mCurr = mCurr->mPrev;
        }
        return static_cast<DerivedType&>(*this);
    }
    const DerivedType operator-- (int) {
        DerivedType result = static_cast<DerivedType&>(*this);
        --*this;
        return result;
    }

    /* Equality and disequality operators are parameterized - we'll allow anyone
   * whose type is IteratorBase to compare with us.  This means that we can
   * compare both iterator and const_iterator against one another.
   */
    template <typename DerivedType2, typename Pointer2, typename Reference2>
    bool operator== (const IteratorBase<DerivedType2, Pointer2, Reference2>& rhs) {
        /* Just check the underlying pointers, which (fortunately!) are of the
     * same type.
     */
        return mOwner == rhs.mOwner && mCurr == rhs.mCurr;
    }
    template <typename DerivedType2, typename Pointer2, typename Reference2>
    bool operator!= (const IteratorBase<DerivedType2, Pointer2, Reference2>& rhs) {
        /* We are disequal if equality returns false. */
        return !(*this == rhs);
    }

    Reference operator* () const {
        return mCurr->mValue;
    }

    Pointer operator-> () const {
        /* Use the standard "&**this" trick to dereference this object and return
         * a pointer to the referenced value*/
        return &**this;
    }

protected:
    /* Which Treap we belong to.  This pointer is const even though we are
   * possibly allowing ourselves to modify the treap elements to avoid having
   * to duplicate this logic once again for const vs. non-const iterators.
   */
    const Treap* mOwner;
    Node* mCurr;/* Where we are in the list. */

    /* In order for equality comparisons to work correctly, all IteratorBases
   * must be friends of one another.
   */
    template <typename Derived2, typename Pointer2, typename Reference2>
    friend class IteratorBase;

    IteratorBase(const Treap* owner = NULL, Node* curr = NULL)
        : mOwner(owner), mCurr(curr) {}
};



template <typename Key, typename Value, typename Comparator>
class Treap<Key, Value, Comparator>::iterator:
        public std::iterator< std::bidirectional_iterator_tag,
        std::pair<const Key, Value> >,
        public IteratorBase<iterator,
        std::pair<const Key, Value>*,
        std::pair<const Key, Value>&>
{
public:
    iterator() {}
private:
    /* Constructor for creating an iterator out of a raw node just forwards this
   * argument to the base type.  This line is absolutely awful because the
   * type of the base is so complex.
   */
    iterator(const Treap* owner,
             typename Treap<Key, Value, Comparator>::Node* node) :
        IteratorBase<iterator,
        std::pair<const Key, Value>*,
        std::pair<const Key, Value>&>(owner, node) {}

    /* Make the Treap a friend so it can call this constructor. */
    friend class Treap;

    /* Make const_iterator a friend so we can do iterator-to-const_iterator conversions. */
    friend class const_iterator;
};


template <typename Key, typename Value, typename Comparator>
class Treap<Key, Value, Comparator>::const_iterator:
        public std::iterator< std::bidirectional_iterator_tag,
        const std::pair<const Key, Value> >,
        public IteratorBase<const_iterator,                       // Our type
        const std::pair<const Key, Value>*,   // Reference type
        const std::pair<const Key, Value>&> { // Pointer type
public:
    const_iterator() {}

    /* iterator conversion constructor forwards the other iterator's base fields
   * to the base class.
   */
    const_iterator(iterator itr) :
        IteratorBase<const_iterator,
        const std::pair<const Key, Value>*,
        const std::pair<const Key, Value>&>(itr.mOwner, itr.mCurr) {}

private:
    const_iterator(const Treap* owner,
                   typename Treap<Key, Value, Comparator>::Node* node) :
        IteratorBase<const_iterator,
        const std::pair<const Key, Value>*,
        const std::pair<const Key, Value>&>(owner, node) {}

    friend class Treap;
};


















template <typename Key, typename Value, typename Comparator>
std::pair<typename Treap<Key, Value, Comparator>::iterator, bool>
Treap<Key, Value, Comparator>::insert(const Key& key, const Value& value) {
    /* Recursively walk down the tree from the root, looking for where the value
   * should go.  In the course of doing so, we'll maintain some extra
   * information about the node's successor and predecessor so that we can
   * wire the new node in in O(1) time.
   *
   * The information that we'll need will be the last nodes at which we
   * visited the left and right child.  This is because if the new node ends
   * up as a left child, then its predecessor is the last ancestor on the path
   * where we followed its right pointer, and vice-versa if the node ends up
   * as a right child.
   */
    Node* lastLeft = NULL, *lastRight = NULL;

    /* Also keep track of our current location as a pointer to the pointer in
   * the tree where the node will end up, which allows us to insert the node
   * by simply rewiring this pointer.
   */
    Node** curr   = &mRoot;

    /* Also track the last visited node. */
    Node*  parent = NULL;

    /* Now, do a standard binary tree insert.  If we ever find the node, we can
   * stop early.
   */
    while (*curr != NULL) {
        /* Update the parent to be this node, since it's the last one visited. */
        parent = *curr;

        /* Check whether we belong in the left subtree. */
        if (mComp(key, (*curr)->mValue.first)) {
            lastLeft = *curr;
            curr = &(*curr)->mChildren[0];
        }
        /* ... or perhaps the right subtree. */
        else if (mComp((*curr)->mValue.first, key)) {
            lastRight = *curr; // Last visited node where we went right.
            curr = &(*curr)->mChildren[1];
        }
        /* Otherwise, the key must already exist in the tree, and we can just
     * return an iterator to it.
     */
        else
            return std::make_pair(iterator(this, *curr), false);
    }

    /* At this point we've found our insertion point and can create the node
   * we're going to wire in.  We'll assign it a random priority.
   */
    Node* toInsert = new Node(key, value, rand());

    /* Splice it into the tree. */
    toInsert->mParent = parent;
    *curr = toInsert;

    /* The new node has no children. */
    toInsert->mChildren[0] = toInsert->mChildren[1] = NULL;

    /* Wire this node into the linked list in-between its predecessor and
   * successor in the tree.  The successor is the last node where we went
   * left, and the predecessor is the last node where we went right.
   */
    toInsert->mNext = lastLeft;
    toInsert->mPrev = lastRight;

    /* Update the previous pointer of the next entry, or change the list tail
   * if there is no next entry.
   */
    if (toInsert->mNext)
        toInsert->mNext->mPrev = toInsert;
    else
        mTail = toInsert;

    /* Update the next pointer of the previous entry similarly. */
    if (toInsert->mPrev)
        toInsert->mPrev->mNext = toInsert;
    else
        mHead = toInsert;

    /* At this point, the node is in the right spot in the tree, and all that
   * remains is to reheapify with tree rotations.  We do this by continuously
   * rotating the tree while the node's parent's priority is greater than the
   * node's priority.
   */
    while (toInsert->mParent && toInsert->mParent->mPriority > toInsert->mPriority)
        rotateUp(toInsert);

    /* Increase the size of the tree, since we just added a node. */
    ++mSize;

    /* Hand back an iterator to the new element, along with a notification that
   * it was inserted correctly.
   */
    return std::make_pair(iterator(this, toInsert), true);
}

/* To perform a tree rotation, we identify whether we're doing a left or
 * right rotation, then rewrite pointers as follows:
 *
 * In a right rotation, we do the following:
 *
 *      B            A
 *     / \          / \
 *    A   2   -->  0   B
 *   / \              / \
 *  0   1            1   2
 *
 * In a left rotation, this runs backwards.
 *
 * The reason that we've implemented the nodes as an array of pointers rather
 * than using two named pointers is that the logic is symmetric.  If the node
 * is its left child, then its parent becomes its right child, and the node's
 * right child becomes the parent's left child.  If the node is its parent's
 * right child, then the node's parent becomes its left child and the node's
 * left child becomes the parent's right child.  In other words, the general
 * formula is
 *
 * If the node is its parent's SIDE child, then the parent becomes that node's
 * OPPOSITE-SIDE child, and the node's OPPOSITE-SIDE child becomes the
 * parent's SIDE child.
 *
 * This code also updates the root if the tree root gets rotated out.
 */
template <typename Key, typename Value, typename Comparator>
void Treap<Key, Value, Comparator>::rotateUp(Node* node) {
    /* Determine which side the node is on.  It's on the left (side 0) if the
   * parent's first pointer matches it, and is on the right (side 1) if the
   * node's first pointer doesn't match it.  This is, coincidentally, whether
   * the node is not equal to the first pointer of its root.
   */
    const int side = (node != node->mParent->mChildren[0]);

    /* The other side is the logical negation of the side itself. */
    const int otherSide = !side;

    /* Cache the displaced child and parent of the current node. */
    Node* child  = node->mChildren[otherSide];
    Node* parent = node->mParent;

    /* Shuffle pointers around to make the node the parent of its parent. */
    node->mParent = parent->mParent;
    node->mChildren[otherSide] = parent;

    /* Shuffle around pointers so that the parent takes on the displaced
   * child.
   */
    parent->mChildren[side] = child;
    if (child)
        child->mParent = parent;

    /* Update the grandparent (if any) so that its child is now the rotated
   * element rather than the parent.  If there is no grandparent, the node is
   * now the root.
   */
    if (parent->mParent) {
        const int parentSide = (parent != parent->mParent->mChildren[0]);
        parent->mParent->mChildren[parentSide] = node;
    } else
        mRoot = node;

    /* In either case, change the parent so that it now treats the node as the
   * parent.
   */
    parent->mParent = node;
}





/* To remove an element, we locate its position in the tree, rotate it down
 * until it becomes a leaf, and then remove it from the tree.
 */
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::iterator
Treap<Key, Value, Comparator>::erase(iterator where) {
    /* Grab the node out of the iterator so we know where to start. */
    Node* node = where.mCurr;

    /* As long as the node has children, keep rotating the node's smaller child
   * upward.
   */
    while (node->mChildren[0] || node->mChildren[1]) {
        /* See which child to rotate upward.  There are four cases. */
        Node* toRotate;

        /* Case one: Only left child. */
        if (!node->mChildren[1])
            toRotate = node->mChildren[0];
        /* Case two: Only right child. */
        else if (!node->mChildren[0])
            toRotate = node->mChildren[1];
        /* Case 3: Both children, left has lower priority. */
        else if (node->mChildren[0]->mPriority < node->mChildren[1]->mPriority)
            toRotate = node->mChildren[0];
        /* Case 4: Both children, right has lower priority. */
        else
            toRotate = node->mChildren[1];

        /* Rotate it up! */
        rotateUp(toRotate);
    }

    /* Break the node out of the tree by cutting the connection to the
   * parent.
   */
    if (node->mParent) {
        /* Use our standard trick to clear the proper field. */
        node->mParent->mChildren[node->mParent->mChildren[0] != node] = NULL;
    }
    /* If there is no parent, the root was just cleared. */
    else
        mRoot = NULL;

    /* Next, we need to splice this node out of the list of entries. */

    /* If there is a next node, wire its previous pointer around the current
   * node.  Otherwise, the tail just changed.
   */
    if (node->mNext)
        node->mNext->mPrev = node->mPrev;
    else
        mTail = node->mPrev;

    /* If there is a previous node, wite its next pointer around the current
   * node.  Otherwise, the head just changed.
   */
    if (node->mPrev)
        node->mPrev->mNext = node->mNext;
    else
        mHead = node->mNext;

    /* Cache the next node; it's the value we're going to be returning. */
    Node* next = node->mNext;

    /* Delete the node and drop the number of nodes left, since we just got rid
   * of something.
   */
    delete node;
    --mSize;

    /* Finally, return an iterator to the next element. */
    return iterator(this, next);
}

/* Erasing a single value just calls find to locate the element and the
 * iterator version of erase to remove it.
 */
template <typename Key, typename Value, typename Comparator>
bool Treap<Key, Value, Comparator>::erase(const Key& key) {
    /* Look up where this node is, then remove it if it exists. */
    iterator where = find(key);
    if (where == end()) return false;

    erase(where);
    return true;
}













/* standard BST lookup */
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::Node*
Treap<Key, Value, Comparator>::findNode(const Key& key) const {
    Node* curr = mRoot;
    while (curr != nullptr) {
        if (mComp(key, curr->mValue.first))
            curr = curr->mChildren[0];
        else if (mComp(curr->mValue.first, key))
            curr = curr->mChildren[1];
        else
            return curr;
    }
    return nullptr;
}


/* wrapping the result up in the appropriate iterator type.*/
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::iterator
Treap<Key, Value, Comparator>::find(const Key& key) {
    return iterator(this, findNode(key));
}
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::const_iterator
Treap<Key, Value, Comparator>::find(const Key& key) const {
    return const_iterator(this, findNode(key));
}




template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::iterator
Treap<Key, Value, Comparator>::begin() {
    return iterator(this, mHead);
}
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::const_iterator
Treap<Key, Value, Comparator>::begin() const {
    return iterator(this, mHead);
}
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::iterator
Treap<Key, Value, Comparator>::end() {
    return iterator(this, NULL);
}
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::const_iterator
Treap<Key, Value, Comparator>::end() const {
    return iterator(this, NULL);
}


template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::reverse_iterator
Treap<Key, Value, Comparator>::rbegin() {
    return reverse_iterator(end());
}
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::const_reverse_iterator
Treap<Key, Value, Comparator>::rbegin() const {
    return const_reverse_iterator(end());
}
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::reverse_iterator
Treap<Key, Value, Comparator>::rend() {
    return reverse_iterator(begin());
}
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::const_reverse_iterator
Treap<Key, Value, Comparator>::rend() const {
    return const_reverse_iterator(begin());
}













/* Cloning a tree is a simple structural recursion. */
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::Node*
Treap<Key, Value, Comparator>::cloneTree(Node* toClone, Node* parent) {
    /* Base case: the clone of the empty tree is that tree itself. */
    if (toClone == NULL) return NULL;

    /* Create a copy of the node, moving over the priorities and key/value
   * pair.
   */
    Node* result = new Node(toClone->mValue.first, toClone->mValue.second,
                            toClone->mPriority);

    /* Recursively clone the subtrees. */
    for (int i = 0; i < 2; ++i)
        result->mChildren[i] = cloneTree(toClone->mChildren[i], result);

    /* Set the parent. */
    result->mParent = parent;

    return result;
}

/* Fixing up the doubly-linked list is a bit tricky.  The function acts as an
 * inorder traversal.  We first fix up the left subtree, getting a pointer to
 * the node holding the largest value in that subtree (the predecessor of this
 * node).  We then chain the current node into the linked list, then fix up
 * the nodes to the right (which have the current node as their predecessor).
 */
template <typename Key, typename Value, typename Comparator>
typename Treap<Key, Value, Comparator>::Node*
Treap<Key, Value, Comparator>::rethreadLinkedList(Node* root, Node* predecessor) {
    /* Base case: if the root is null, then the largest element visited so far
   * is whatever we were told it was.
   */
    if (root == NULL) return predecessor;

    /* Otherwise, recursively fix up the left subtree using the actual
   * predecessor.  Store the return value as the new predecessor.
   */
    predecessor = rethreadLinkedList(root->mChildren[0], predecessor);

    /* Add ourselves to the linked list. */
    root->mPrev = predecessor;
    if (predecessor)
        predecessor->mNext = root;
    root->mNext = NULL;

    /* Recursively invoke on the right subtree, passing in this node as the
   * predecessor.
   */
    return rethreadLinkedList(root->mChildren[1], root);
}








#endif // TREAP_H

